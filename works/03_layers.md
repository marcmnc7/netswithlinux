#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

	$ ip r f all
	$ ip a f dev enp2s0
	$ ip a
	[...]
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:d3 brd ff:ff:ff:ff:ff:ff

	$ip r
	[no contesta nada]	
		
	
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	ip a a  2.2.2.2/24 dev enp2s0
	ip a a  3.3.3.3/16 dev enp2s0
	ip a a	4.4.4.4/25 dev enp2s0
	ip a s dev enp2s0
	
	[salida]
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:d3 brd ff:ff:ff:ff:ff:ff
    inet 2.2.2.2/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp2s0
       valid_lft forever preferred_lft forever

Consulta la tabla de rutas de tu equipo

	ip r
	
	[salida]
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 


Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	2.2.2.2 --> OK porque tengo la ip 2.2.2.2/24 y esta dentro de la misma red.
	2.2.2.254 --> From 2.2.2.2 icmp_seq=2 Destination Host Unreachable porque nadie tiene esta ip o el ordenador esta apagado, por ejemplo.  
	2.2.5.2 --> Connect: Network is unreachable. Porque no estamos en la misma red.
	3.3.3.35 --> From 3.3.3.3 icmp_seq=2 Destination Host Unreachable. Porque el ordenador esta apagado, por ejemplo. 
	3.3.200.45 --> From 3.3.3.3 icmp_seq=2 Destination Host Unreachable. Porque el ordenador esta apagado, por ejemplo. 
	4.4.4.8 --> From 4.4.4.4 icmp_seq=1 Destination Host Unreachable. Porque el ordenador esta apagado, por ejemplo. 
	4.4.4.132 --> Connect: Network is unreachable. Porque no estamos en la misma red.

#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet
	
	$ ip a f dev enp2s0
	$ ip a
	[...]
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:d3 brd ff:ff:ff:ff:ff:ff

Conecta una segunda interfaz de red por el puerto usb
	ip a
	3: enp0s29u1u1c2: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1b:7d brd ff:ff:ff:ff:ff:ff


Cambiale el nombre a tarjetared
	ip link set enp0s29u1u1c2 down
	ip link set enp0s29u1u1c2 name tarjetared
	ip link set tarjetared up
	ip a
	[...]
	3: tarjetared: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1b:7d brd ff:ff:ff:ff:ff:ff

Modifica la dirección MAC
	ip link set tarjetared down
	ip link set tarjetared address 00:11:22:33:44:55
	ip link set tarjetared up
	ip a
	[...]
	3: tarjetared: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	ip a a 5.5.5.5/24 dev tarjetared
	ip a a 7.7.7.7/24 dev enp2s0
	ip a
	[...]
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:d3 brd ff:ff:ff:ff:ff:ff
    inet 7.7.7.7/24 scope global enp2s0
       valid_lft forever preferred_lft forever
	3: tarjetared: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
		inet 5.5.5.5/24 scope global tarjetared
			valid_lft forever preferred_lft forever

Observa la tabla de rutas
	
	ip r
	[salida]
	5.5.5.0/24 dev tarjetared  proto kernel  scope link  src 5.5.5.5 linkdown 
	7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7 


#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	$ ip a f dev enp2s0
	$ ip a
	[...]
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:d3 brd ff:ff:ff:ff:ff:ff


En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)
	ip a a 172.16.99.18/24 dev enp2s0


Lanzar iperf en modo servidor en cada ordenador
	
	iperf -s
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)

Comprueba con netstat en qué puerto escucha
	netstat -utlnp
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      5384/iperf          
	         
Conectarse desde otro pc como cliente
	
	En el otro ordenador: iperf -c 170.16.99.18
	
	En mi ordenador:
	local 172.16.99.18 port 5001 connected with 172.16.99.17 port 36882


Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

	tshark -i enp2s0 -c30 "port 5001"


Encontrar los 3 paquetes del handshake de tcp

	tshark -i enp2s0 -c3 "port 5001"
	Capturing on 'enp2s0'
	[salida]
	1 0.000000000 172.16.99.17 → 172.16.99.18 TCP 74 36892→5001 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM=1 TSval=12697967 TSecr=0 WS=128
	2 0.000036076 172.16.99.18 → 172.16.99.17 TCP 74 5001→36892 [SYN, ACK] Seq=0 Ack=1 Win=28960 Len=0 MSS=1460 SACK_PERM=1 TSval=13017200 TSecr=12697967 WS=128
	3 0.000112836 172.16.99.17 → 172.16.99.18 TCP 66 36892→5001 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=12697967 TSecr=13017200
	3 packets captured


Abrir dos servidores en dos puertos distintos

	En dos terminales distintos:
	iperf -s -p 5002
	iperf -s -p 5001
	
Observar como quedan esos puertos abiertos con netstat

	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      5807/iperf          
	tcp        0      0 0.0.0.0:5002            0.0.0.0:*               LISTEN      5814/iperf   

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

	En los otros PC:
	iperf -c 172.16.99.18 -t 60
	iperf -c 172.16.99.18 -t 60
	[salida]
	Interval       Transfer     Bandwidth
	0.0-60.0 sec   674 MBytes  94.2 Mbits/sec

		
	En mi PC:
	iperf -s
	[salida]
	Interval       Transfer     Bandwidth
	0.0- 3.0 sec  17.0 MBytes  47.4 Mbits/sec
	0.0-60.0 sec   657 MBytes  91.8 Mbits/sec


Mientras tanto con netstat mirar conexiones abiertas
	netstat -utlnp


