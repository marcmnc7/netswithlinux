# 1. Instalar wireshark
Ejecuta:

```
sudo dnf -y install wireshark-gnome wireshark-cli
```
Arranca wireshark y realiza 3 capturas que se guardarán en 3 ficheros con extensión .pcap y nombres:
- 00_ping.pcap
- 00_http.pcap
- 00_https.pcap

Las 3 capturas han de recoger el tráfico mientras:

a. Ping a la ip del ordenador de profe

b. Arrancar el firefox y escribir la dirección http://netfilter.org

c. Arrancar el firefox y escribir la dirección https://kernel.org


# 2. Analiza con wireshark el tráfico
### Abre **00_ping.pcap** y contesta:
#### a. Que pila de protocolos se usa para hacer un ping

La pila de protocolos que se usan para hacer un ping són:
- IPv4 (Internet Protocol version 4)
- ICMP (Internet Control Message Protocol)

#### b. Cual es el tamaño de un paquete de ping

El tamaño de un paquete de ping es de 784 bits.

#### c. Cuantos bytes se dedican a la cabecera ethernet

Se dedican 14 bytes a la cabecera de internet.

#### d. Cuantos bytes se dedican a la cabecera IP

Se dedican 20 bytes a la cabecera IP.

#### e. Cual es la dirección mac del equipo del profe

La dirección MAC del equipo del profe es 94:de:80:49:7d:fd

#### f. Hay alguna trama de broadcast?? que protocolos usa??

Si. Utiliza el protocolo ARP.

#### g. Escribe en binario y hexadecimal la dirección IP

Hexadecimal: c0 a8 00 13

Binario: 11000000 10101000 00000000 00010011


### Abre **00_http.pcap** y contesta:
#### h. Que pila de protocolos se usa en un paquete con http

La pila de protocolos que se usan són:
- HTTP (Hiper Text Transfer Protocol)
- TCP (Transmssion Control Protocol)
- IPv4 (Internet Protocol version 4)


#### i. Cual es la dirección ip del servidor web

La direccion IP del servidor web es: 150.214.142.167

#### j. Cual es la dirección mac destino de un paquete que va hacia una ip pública

La dirección MAC destino de un paquete que va hacia una ip pública es 60:a4:4c:b1:f4:ed

#### k. ¿Puedes encontrar en un paquete de respuesta del servidor web cual es el título de la página web? Explica que has hecho para encontrarlo

Si, se puede encontrar siguiendo los siguientes pasos:

- Filtro por http.
- Seleccionar un paquete de respuesta del servidor.
- Abrir protocolo HTTP. 
- Pulsar en file data.
- Buscar en los ASCII.


#### l. ¿Cúantos paquetes se han recibido desde la ip del servidor?

Se han recibido 113 paquetes desde la ip del servidor.


### Abre **00_https.pcap** y contesta:
#### m. Que pila de protocolos se usa en un paquete con http

La pila de protocolos que se usan són:
- OCSP (Online Certificate Status Protocol)
- HTTP (Hiper Text Transfer Protocol)
- TCP (Transmision Control Protocol)
- IPv4 (Internet Protocol version 4)

#### n. Cual es la dirección ip del servidor web

La direccion ip del servidor web es 185.43.180.227


#### o. Cual es la dirección mac destino de un paquete que va hacia una ip pública

La direccion MAC destino de un paquete que va hacia una ip pública es 60:a4:4c:b1:f4:ed


#### p. En este caso no se puede encontrar en un paquete de respuesta del servidor web cual es el título de la página web. Explica por qué crees que no se encuentra.

Porque la información viene cifrada, ya que es https.


#### q. ¿Cúantos paquetes se han recibido desde la ip del servidor?

Se han recibido un total de 4 paquetes del servidor.

# 3. Realizar una captura con tshark
#### a. Capturar el tráfico entrante por la interfaz de red que usamos para conectarnos a internet y que lo guarde en un fichero /tmp/out1.pcap hasta que decidamos interrumpir la captura con Ctrl + C*

tshark -w /tmp/out1.pcap

#### b. Capturar el tráfico entrante durante 10 segundos

tshark -a duration:10

#### c. Capturar el tráfico entrante hasta llegar a 100 paquetes
tshark -c 100


# 4. Construye filtros:
### Abre **00_http.pcap** y escribe el filtro que has de utilizar para seleccionar:
#### a. Encontrar sólo los paquetes que lleven el protocolo HTTP

http

#### b. Encontrar sólo los paquetes que tienen como ip origen una dirección de la familia 192.168.0.0/16

ip.src == 192.168.0.0/16

#### c. Combinar los dos filtros anteriores para que sólo busque los paquetes que llevan cabeceras del protocolo HTTP y que tengan como ip origen una dirección de la familia 192.168.0.0/16. Guarda sólo estos paquetes filtrados en un nuevo fichero que se llame 00_http_get.pcap

DONE!

### Abre **00_ping.pcap** y aplica un filtro para :
#### d. Buscar las tramas broadcast ethernet

eth.dst == ff:ff:ff:ff:ff:ff

#### e. Aplica un filtro al realizar la captura

```
tshark -w ~/00_broadcast.pcap -a duration:10 | tshark -Y "eth.dst == ff:ff:ff:ff:ff:ff" -r ~/00_broadcast.pcap
```

Hay que conseguir que tshark sólo capture las tramas ethernet broadcast y guarde el fichero como **00_broadcast.pcap**

