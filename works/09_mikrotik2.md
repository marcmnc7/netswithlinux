EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt00Marc
/system backup save name="20170316_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

Para resetear el router, se puede hacer de dos formas.
	-A) Accediendo via telnet:
			o $] telnet 192.168.88.1
			o $] /system reset-configuration
	-B) Pulsando en botón reset del mikrotik.
	
Restauramos el backup con: 
	o /system backup load name="20170316_zeroconf"
	o Nos pregunta si queremos reboot. Aceptamos. 
	o Volvemos a entrar. Tarda un poco. 

El backup esta guardado en el directoria /file. Podemos ver todos los 
archivos guardados en este directorio con la orden: /file print. 

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*118: red pública
*218: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free118" 
/interface wireless set 0 name="wFree"
/interface wireless add ssid="private218" name="wPrivate" master-interface=wFree 
```
Con la primer orden, cambiamos el nombre de la interfaz wireless 0 a free118s
Con la segunda orden, añadimos una interface wlan1 con el nombre private218


###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

Las interfaces wifi aparecen como disabled porque en la configuracion del
backup teniamos puesta esta orden:
--> /interface wireless set [ find default-name=wlan1 ] disabled=yes
Esta orden deshabilita la wireless con el nombre wlan1. La wlan2
tambien aparece como disabled porque la hemos creado nueva despues.  

## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan118 vlan-id=118 interface=eth4
/interface vlan add name eth4-vlan218 vlan-id=218 interface=eth4   
                                   
/interface bridge add name=br-vlan118
/interface bridge add name=br-vlan218

/interface bridge port add interface=eth4-vlan118 bridge=br-vlan118
/interface bridge port add interface=eth3 bridge=br-vlan118
/interface bridge port add interface=wFree  bridge=br-vlan118      

/interface bridge port add interface=eth4-vlan218 bridge=br-vlan218
/interface bridge port add interface=wPrivate   bridge=br-vlan218

/interface print

```

### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado

Primero añadimos dos interfaces eth4 con id's 118 i 218 i nombres 
especificados (eth4-vlan118 i eth:

```
/interface vlan add name eth4-vlan118 vlan-id=118 interface=eth4
/interface vlan add name eth4-vlan218 vlan-id=218 interface=eth4   
```                             

Añadimos dos briges con los nombres especificados:

```
/interface bridge add name=br-vlan118
/interface bridge add name=br-vlan218
```

Conectamos cada interface (vlan, eth3 i wFree) al Bridge vlan118 para 
que pueda interactuar con ellas:

```
/interface bridge port add interface=eth4-vlan118 bridge=br-vlan118
/interface bridge port add interface=eth3 bridge=br-vlan118
/interface bridge port add interface=wFree  bridge=br-vlan118      
```

Ahora, para el bridge vlan218 conectamos solo dos interfaces, la 
eth4-vlan 218 y la wPrivate, para que pueda interactuar con ellas:

```
/interface bridge port add interface=eth4-vlan218 bridge=br-vlan218
/interface bridge port add interface=wPrivate   bridge=br-vlan218
```

Mostramos toda la configuracion actual de las interfaces, para comprovar
que se ha hecho correctamente:

```
/interface print
```

### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración

Añadimos las ip's a los bridges:
```
ip address add address=172.17.118.1/24 interface=br-vlan118
ip address add address=172.17.218.1/24 interface=br-vlan218   
```

Ahora, dentro de la placa base del linux nos añadimos ips de estas 
familias.

```
ip a a 172.17.118.50/24 dev enp2s0
ip a a 172.17.218.50/24 dev enp2s0
```
Comprobamos que el ping funciona:

```
ping 172.17.118.1
```

Salida del ping.. OK! 

```
PING 172.17.118.1 (172.17.118.1) 56(84) bytes of data.
64 bytes from 172.17.118.1: icmp_seq=1 ttl=64 time=0.380 ms
64 bytes from 172.17.118.1: icmp_seq=2 ttl=64 time=0.238 ms
64 bytes from 172.17.118.1: icmp_seq=3 ttl=64 time=0.211 ms
^C
--- 172.17.118.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1999ms
rtt min/avg/max/mdev = 0.211/0.276/0.380/0.075 ms

```

Ahora, faltaria configurar para que funcione conectando al puerto 4
del mikrotik. Para hacerlo, se tiene que añadir una vlan a la placa
base del linux, para que lo mande con etiquetas.

```
ip link add link enp2s0 name vlanlinux type vlan id 218
ip link add link enp2s0 name vlanlinux2 type vlan id 118
ip a a 172.17.218.50/24 dev vlanlinux
ip a a 172.17.118.50/24 dev vlanlinux2
ip link set vlanlinux up
ip link set vlanlinux2 up
ping 172.17.218.1
ping 172.17.118.1
```
OK!

Finalmente, guardamos el backup!

```
/system backup save name="20170316_ej4"
```

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en 
los rangos .101 a .250 de las respectivas redes. Asignar ip manual a la 
eth1. Crear reglas de nat para salir a internet. 

```
/ip pool add ranges=172.17.118.100-172.17.118.200 name=rang_public

/ip pool add ranges=172.17.218.100-172.17.218.200 name=rang_private

/ip dhcp-server add address-pool=rang_public
interface: br-vlan118

/ip dhcp-server add address-pool=rang_private
interface: br-vlan218

/ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.118.1 netmask=24 domain=MKT18.com address=172.17.118.0/24 

/ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.218.1 netmask=24 domain=MKT18.com address=172.17.218.0/24  

/ip dhcp-server set 0 disabled=no

/ip dhcp-server set 1 disabled=no

/interface wireless set ssid=mktf18
numbers: 0

/interface wireless set ssid=mktp18 
numbers: 1

```

#NAT#

```

/ip address add address=192.168.3.218/16 interface=eth1
/ip firewall nat add action=masquerade chain=scrnat out-interface=eth1
/ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1
/ip firewall filter add chain=input action=accept protocol=tcp dst-port=22 in-interface=eth1 place-before=0

```

### [ejercicio6] Activar redes wifi y dar seguridad wpa2

Activamos la wifi free i la private
```

/interface wireless set 0 disabled=no 
/interface wireless set 1 disabled=no
 
```

Añadimos un nuevo perfil que se le asignara a la wifi private
```
/interface wireless security-profile add name=perfilprivate        
```

Le ponemos contraseña a este perfil de seguridad y le activamos la 
seguridad:
```
/interface wireless security-profile set wpa2-pre-shared-key=mktp18 
numbers: 1

/interface wireless security-profiles set 1 authentication-types=wpa2-psk
/interface wireless security-profiles set 1 mode=static-keys-required 
```

Asignamos el nuevo perfil creado a la wifi private
```
/interface wireless set security-profile=perfilprivate
numbers: 1
```

Comprovamos con print que todo esta correctamente, primero de las wifis:
```
[admin@mkt18Marc] > /interface wireless print                        
Flags: X - disabled, R - running 
 0    name="wFree" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:BF:10:3A 
      arp=enabled interface-type=Atheros AR9300 mode=ap-bridge 
      ssid="mktf18" frequency=auto band=2ghz-b/g/n 
      channel-width=20/40mhz-Ce scan-list=default 
      wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 
      wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no 
      bridge-mode=enabled default-authentication=yes 
      default-forwarding=yes default-ap-tx-limit=0 
      default-client-tx-limit=0 hide-ssid=no 
      security-profile=default compression=no 

 1    name="wPrivate" mtu=1500 l2mtu=1600 
      mac-address=6E:3B:6B:BF:10:3A arp=enabled 
      interface-type=virtual-AP master-interface=wFree 
      ssid="mktp18" vlan-mode=no-tag vlan-id=1 wds-mode=disabled 
      wds-default-bridge=none wds-ignore-ssid=no 
      bridge-mode=enabled default-authentication=yes 
      default-forwarding=yes default-ap-tx-limit=0 
      default-client-tx-limit=0 hide-ssid=no 
      security-profile=perfilprivate 
```

Y despues print de los perfiles de seguridad creados:

```
[admin@mkt18Marc] > /interface wireless security-profiles print
Flags: * - default 
 0 * name="default" mode=none 
     authentication-types="" unicast-ciphers=aes-ccm 
     group-ciphers=aes-ccm wpa-pre-shared-key="" 
     wpa2-pre-shared-key="" supplicant-identity="MikroTik" 
     eap-methods=passthrough tls-mode=no-certificates 
     tls-certificate=none mschapv2-username="" 
     mschapv2-password="" static-algo-0=none static-key-0="" 
     static-algo-1=none static-key-1="" static-algo-2=none 
     static-key-2="" static-algo-3=none static-key-3="" 
     static-transmit-key=key-0 static-sta-private-algo=none 
     static-sta-private-key="" radius-mac-authentication=no 
     radius-mac-accounting=no radius-eap-accounting=no 
     interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX 
     radius-mac-mode=as-username radius-mac-caching=disabled 
     group-key-update=5m management-protection=disabled 
     management-protection-key="" 

 1   name="perfilprivate" mode=static-keys-required 
     authentication-types=wpa2-psk unicast-ciphers=aes-ccm 
     group-ciphers=aes-ccm wpa-pre-shared-key="" 
     wpa2-pre-shared-key="mktp18" supplicant-identity="mkt18Marc" 
     eap-methods=passthrough tls-mode=no-certificates 
     tls-certificate=none mschapv2-username="" 
     mschapv2-password="" static-algo-0=none static-key-0="" 
     static-algo-1=none static-key-1="" static-algo-2=none 
     static-key-2="" static-algo-3=none static-key-3="" 
     static-transmit-key=key-0 static-sta-private-algo=none 
     static-sta-private-key="" radius-mac-authentication=no 
     radius-mac-accounting=no radius-eap-accounting=no 
     interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX 
     radius-mac-mode=as-username radius-mac-caching=disabled 
     group-key-update=5m management-protection=disabled 
     management-protection-key=""
```
OK!

###############################################################################################################################################
------------- A PARTIR D'AQUEST PUNT--> Vaig fer export de configuracio de router Adrian i la vaig importar a jo. Aixi funciona. --------------
###############################################################################################################################################

###########################################################################
###################################################### de Adr. NO EXECUTAT#
#AQUI COPIA DE FILE: SECURITY MIKROTIK 08

## SECURITY ##
## Cambiamos el puerto donde escucha el ssh
```
/ip service
set ssh port=22340
```
## Permitimos que desde cualquier ip se puedan conectar a los puertos
del router que queramos
```
/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp 
```
## Si quiero añadir esa regla la primera de todas si ya disponemos
 de otras reglas usamos place-before
```
/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp place-before=0
```
## Cambiamos el password (si no tiene no hace falta poner old-password)
```
/password new-password=clase confirm-new-password=clase
```
## Sincronizamos el reloj del router usando el protocolo ntp a 
partir de alguna ip de los servidores de tiempo. En un linux:
```
/system ntp client
set enabled=yes primary-ntp=213.251.52.234 secondary-ntp=158.227.98.15
```
### [ejercicio6b] Opcional (monar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.
```
add action=drop chain=forward comment="defconf: DROP ICMP" dst-address=\
    172.17.217.0/24 src-address=172.17.117.0/24
```
################################################################################
###################################################################################

### [ejercicio8] Conexión a switch cisco con vlans



### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante



### [ejercicio9] QoS



### [ejercicio9] Balanceo de salida a internet



### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos



### [ejercicio11] Firewall avanzado

