# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).

IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.16.4.0 | 172.16.4.127 | .1 a .126
174.187.55.6/23 | 170.16.54.0 | 170.16.55.255 | .54.1 a .55.254
10.0.25.253/18 | 10.0.0.0 | 10.0.63.25 | .0.1 a 63.254
209.165.201.30/27 | 209.165.201.0 | 209.165.201.31 | .1 a .30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.
- 32-23=9 bits per hosts, per tant, 9 zeros.
- 2^9 = 512 combinacions possibles, ok! (2^8 = 256, no arriba.. anar provant)
- 512-2 adreçes reservades = 510 hosts (510 > 400 ok!)
- Per tant, mascara serà amb 9 zeros:
    - 11111111.11111111.11111110.00000000 (format binari)
    - 255.255.254.0 (format decimal)

##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

- 32-21=11, per tant, 11 zeros.
- 22^11= 2048 combinacions possibles, ok!
- 2048 - 2 = 2046 hosts
- Per tant, mascara sera amb 11 zeros:
    - 11111111.11111111.11111000.00000000
    - 255.255.248.0
    - 10.192.0.0/21

b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?
- 32-10 = 22 zeros -> 11111111.11000000.00000000.00000000
- 22 - 11 zeros de supernet = 11 
- Posibilitats amb 11 xifres de binary = 2^11 = 2048 subxarxes de 1500 dispositius. 

c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

##### Forma A de pensar-ho:
- Abans: 10.192.0.0/21
- 32-23=9 bits per hosts, per tant, 9 zeros.
- 2^9 = 512 combinacions possibles, ok! (2^8 = 256, no arriba.. anar provant)
- 512-2 adreçes reservaes = 510 hosts (510 > 500 ok!)
- Per tant, mascara serà amb 9 zeros:
    - 11111111.11111111.11111110.00000000 (format binari)
    - 255.255.254.0 (format decimal)
    - 10.192.0.0/23

##### Forma B de pensar-ho: 
- Abans teniem 10.192.0.0/21, es a dir: 
    - 11111111.11111111.11111000.00000000
- Com que fixem xarxa mare amb aquests 11 zeros. Mirem quants en necessitem per fer els 510 hosts: (2^9) -2  = 510. En necessitem 9. 
- Amb aquests dos zeros que falten (11-9), podem fer tres o mes combinacions binaries? Si. (2^2 = 4). 
- Per tant, aquests dos zeros ara serviran per marcar subxaxa.
- Tindrem 4 subxarxes de 510 dispositius cadascuna. OK! 
- Per tant, mascara serà amb 9 zeros:
    - 11111111.11111111.11111110.00000000 (format binari)
    - 255.255.254.0 (format decimal)
    - 10.192.0.0/23


##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius
a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.
- 32-20=12 bits per hosts, per tant, 12 zeros.
- 2^12 = 4096 combinacions possibles, ok!
- 4096-2 adreçes reservades = 4094 hosts
- Per tant, mascara serà amb 12 zeros:
    - 11111111.11111111.11110000.00000000 (format binari)
    - 255.255.240.0 (format decimal)
    - 10.128.0.0/20

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

##### Forma A de pensar-ho: 

- 2046 /20 o dos subredes de 2048  /21 o cuatro subredes de 1024 /22... NO ES POSSIBLE PORQUE la siguiente serian ocho subredes de 512 /23... y queremos entre 4-8 subredes (5 en concreto) y que en estas quepen almenos 650. Por tanto, no tenemos que empezar por un /20, sino por un /19.

##### Forma B de pensar-ho:

- Tenim 10.128.0.0, per tant:
    - 11111111.11111111.11110000.00000000
- Ara necessitem posar 650 dispositius a cada xarxa. Per tant, necessitem minim 10 zeros. (2^9 = 512 NO, 2^10 = 1024 OK, ... )
- Per tant, dels 12 zeros que teniem, en podem emprar nomes 2 (12-10) per subredes. 
- Mirem si amb aquests dos podem fer, crear, 5 subxarxes:
    - 2^2 = 4 NO. 
- Per tant, no es pot amb aquesta xarxa mare.

##### Si així i tot es volgués fer, s'hauria de ampliar la xarxa mare...

- Donar, per tant, una xifra mes per jugar amb les mascares. 
- La mascara mare seria amb 13 zeros:
    - 11111111.11111111.11100000.00000000
    - 255.255.224.0
    - 10.128.0.0/19
- 10 d'aquests per els hosts i 3 per la subxarxes:
    - 11111111.11111111.11111100.00000000
    - 255.255.252.0
    - 10.128.0.0/22

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

- La capacitat maxima que podria assolir cada subxarxa si agafessim els 3 digits que minim neecssitem per fer les 5 subxarxes, seria de 510 hosts. Ja que si agafem els 3 per subxarxa.. 12 zeros totals que teniem, menys 3 de subzarxa, quedarien 9 zeros. Per tant les possibilitats de hosts amb 9 zeros son, (2^9) - 2 = 510 hosts.

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

- Per aconsguir 1625 dispositius, necessitem 11 zeros.
    - (2^11) - 2  = 2046 

- Per tant, amb la mascara mare que tenim a l'apartat A (12 zeros) sols ens quedaria un digit per la subxarxa. Per tant, tenim que veure quantes possibilitats de subxarxa tenim amb 1 digit: 
    - 2^1 = 2 OK

- Efectivament, quedarien 2 subxarxes de 2046 host cadascuna, per tant l'enunciat es compleix. 

- La mascara passaria de /20 a /21.  

##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.
a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

- Tenim 172.16.0.0/12
- 11111111.11110000.00000000.00000000
- Per tenir un marge de 6.. necessitem 2^2=4 NO, 2^3=8 SI. Necessitem 3 bits per poder fer 6 subxarxes. Per tant, de /12 a /15.

b) Dóna la nova MX, tant en format decimal com en binari.
- 172.16.0.0/15
- 11111111.11111110.00000000.00000000
- 255.254.0.0


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 10101100.00010000.00000000.00000000 172.16.0.0 | 10101100.00010001.11111111.11111111 172.17.255.255 | De 172.16.0.1 a 172.17.255.254
Xarxa 2 | 10101100.00010010.00000000.00000000 172.18.0.0 | 10101100.00010011.11111111.11111111 172.19.255.255 | De 172.18.0.1 a 172.19.255.254
Xarxa 3 | 10101100.00010110.00000000.00000000 172.20.0.0 | 10101100.00010111.11111111.11111111 172.21.255.255 | De 172.20.0.1 a 172.21.255.254
Xarxa 4 | 10101100.00011000.00000000.00000000 172.22.0.0 | 10101100.00011001.11111111.11111111 172.23.255.255 | De 172.22.0.1 a 172.23.255.254
Xarxa 5 | 10101100.00011010.00000000.00000000 172.24.0.0 | 10101100.00011011.11111111.11111111 172.25.255.255 | De 172.24.0.1 a 172.25.255.254
Xarxa 6 | 10101100.00011100.00000000.00000000 172.26.0.0 | 10101100.00011101.11111111.11111111 172.27.255.255 | De 172.26.0.1 a 172.27.255.254
 

d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

- 2^3 = 8. Podriem crear 8 subxarxes. 

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

- 11111111.1111 111 0.00000000.00000000
- Cada subxarxa pot tenir 2^17zeros - 2 = 131070 hosts.

##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 4 subxarxes?

- Necessitem 2 bits. 2^2 = 4. Per tant, passem de /24 a /26.
 
b) Dóna la nova MX, tant en format decimal com en binari.

- 192.168.1.0/26
- 11111111.11111111.11111111.11000000
- 255.255.255.192

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 11000000.10101000.00000001.00000000 192.168.1.0 | 11000000.10101000.00000001.00111111 192.168.1.63 | De 192.168.1.1 a 192.168.1.62
Xarxa 2 | 11000000.10101000.00000001.01000000 192.168.1.64 | 11000000.10101000.00000001.01111111 192.168.1.127 | De 192.168.1.65 a 192.168.1.126
Xarxa 3 | 11000000.10101000.00000001.10000000 192.168.1.128 | 11000000.10101000.00000001.10111111 192.168.1.191 | De 192.168.1.129 a 192.168.1.190
Xarxa 4 | 11000000.10101000.00000001.11000000 192.168.1.192 | 11000000.10101000.00000001.11111111 192.168.1.255 | De 192.168.1.193 a 192.168.1.254

d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

- No, maxim podriem fer 4 subxarxes. 2^2 = 4

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

- 11111111.11111111.11111111.11000000
- Cada subxarxa pot tenir 2^6zeros -2 = 62 hosts.
