
### 1. Qué velocidades máximas se alcanzan con los estándares: 802.11a, 802.11b, 802.11g, 802.11n, 802.11ac, 802.11ad y en qué rango de frecuencias trabaja cada estándar.

A partir de la siguiente entrada de Wikipedia:

https://en.m.wikipedia.org/wiki/IEEE_802.11

Interpretar la tabla del apartado protocol




### 2. Qué estándar de seguridad se debe usar. Ordena de menor a mayor la seguridad de los
estándares WEP64, WEP128, WPA1-PSK, WPA1-

Lectura de referencia: http://www.islabit.com/51272/seguridad-wi-fi-deberiamos-usar-wpa2-tkip-wpa2-aes-o-ambos.html

### 3. Qué desventajas a parte de la seguridad tiene usar compatibilidad con TKIP?


### 4. Si se realiza un ataque de fuerza bruta contra WPA2 PSK que recomendación de longitud y combinaciones de tipos de carácteres recomendarías:

Lectura de referencia: https://www.acrylicwifi.com/blog/es-segura-red-wifi-wpa-wpa2/


### 5. Cual es la función de un servidor radius

### 6. Seguridad enterprise. Diferentes sistemas para autenticarse, ¿cual recomendarías?

#### Password-Based Authentication

The vast majority of authentication methods rely on username/password. It’s the easiest to deploy since most institutions already have some sort of credentials set up, but you’re still susceptible to all of the problems of passwords without an onboarding system (see below).

For password based authentication, there are basically 2 options: PEAP and EAP-TTLS. They both are functionally similar, but TTLS is not supported in any Microsoft OS before Windows 8 without using a third party supplicant like our Enterprise Client. At this point, most institutions have deployed or made the switch to PEAP. However, you can’t deploy PEAP without either using Active Directory (a proprietary Microsoft service) or leaving your passwords unencrypted.

#### Token-based Authentication

Tokens used to always be physical devices in the form of key fobs or dongles that would be distributed to users, generating numbers in sync with a server to add extra validation to a connection. But dongles are expensive and get out of sync with the servers from time to time, but at least you could carry them around. They also could have advanced features like fingerprint scanners or plug in with USB.

Physical tokens are still in use, but their popularity is waning as smartphones have made them redundant. What you used to have on a fob can now be put into an app. There are also many other ways to do two-factor authentication outside of the EAP method itself, like using text messages or emails to validate a device.

#### Certificate-Based Authentication

Certificates have long been a mainstay of authentication in general, but have not typically been deployed in BYOD settings since it requires getting users to install them on their own devices. However, once a certificate is installed, they are amazingly convenient. They are not affected by password change policy, they are far safer than username/password, and devices authenticate faster.

